#!/usr/bin/python


import smtplib, feedparser, yaml, datetime
body = ""
now = datetime.datetime.now()
entries = 0

def rssParse(source,entires):
        output = ''
        output += source['title'] + "\n" * 2
        dataSource = feedparser.parse(source['url'])
        for post in dataSource.entries[:entries]:
                output += post.title + ":\n" + post.link + "\n"
        return output

def configLoad(config, value):
        rawYaml = open(config, 'r')
        return yaml.load(rawYaml)[value]

for source in configLoad('./config.yaml','sources'):
        entries = configLoad('./config.yaml','entries')
        body += rssParse(source,int(entries))
        body += "\n" * 3

def sendMailinsecure(to,sender,body,subject,server,port):
        date = now.strftime("%d/%m/%Y %H:%M")
        msg = "From: " + sender + "\nTo: "+ to +"\nSubject: "+subject+"\nDate: " + date + "\n\n" + body + ""
        smtp = smtplib.SMTP()
        smtp.connect(server, port)
        smtp.sendmail(sender, to, msg)
        smtp.quit()
def sendMailsecure(to,sender,body,subject,server,port,username,passwd):
        date = now.strftime("%d/%m/%Y %H:%M")
        msg = "From: " + sender + "\nTo: "+ to +"\nSubject: "+subject+"\nDate: " + date + "\n\n" + body + ""
        smtp = smtplib.SMTP()
        smtp.login(username,passwd)
        smtp.connect(server, port)
        smtp.sendmail(sender, to, msg)
        smtp.quit()


to = configLoad('./config.yaml','to')
sender = configLoad('./config.yaml','from')
server = configLoad('./config.yaml','server')
port = configLoad('./config.yaml','port')
subject = configLoad('./config.yaml','subject')
user = configLoad('./config.yaml','user') 
password = configLoad('./config.yaml','pass')

if user == "" or password == "":
    sendMailInsecure(to.encode('utf-8'),sender.encode('utf-8'),body.encode('utf-8'),subject.encode('utf-8'),server.encode('utf-8'),port)
if user == "null" or password == "null":
    sendMailInsecure(to.encode('utf-8'),sender.encode('utf-8'),body.encode('utf-8'),subject.encode('utf-8'),server.encode('utf-8'),port)

else: 
    sendMailSecure(to.encode('utf-8'),sender.encode('utf-8'),body.encode('utf-8'),subject.encode('utf-8'),server.encode('utf-8'),port,user,password)
