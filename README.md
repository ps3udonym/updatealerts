# UpdateAlerts

## What
Update alerts is a tool that can be leveraged to deliver a concentrated set of posts from your favorite RSS feeds right to your inbox

## Why
I created this to keep up to date on security patches at one of my previous jobs via their respective RSS feeds, I ended up reusing it at another job for the same thing, so I guess it's mostly useful for that. 

## How
The power of python of course. This script hits the RSS feeds described in the config file and sends a perfectly good and simple email to your favorite SMTP server. once that server gets the mail it should land in the configured inbox.  